//
//  NewGameViewController.swift
//  Seq5
//  Contains Invite Friends and Play Friends buttons as well as displays a list of existing games.
//
//  Created by Christopher Serra on 12/23/17.
//  Copyright © 2017 Christopher Serra. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMobileAds

class NewGameViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var buttonInvite: UIButton!
    @IBOutlet weak var buttonFacebookFriend: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var games:Array<Dictionary<String, Any>>! = []
    let defaults = UserDefaults.standard
    var gameTimer: Timer!
    let activitiyViewController = ActivityViewController(message: "Loading Games...", hideIndicator: false)
    var bannerView: GADBannerView!
    @IBOutlet weak var buttonRemoveAds: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // check if they purchased the remove ads IAP
        if !defaults.bool(forKey: "removeAdsPurchaseMade") {
            // if not, create the AdMob advertisement banner
            bannerView = GADBannerView(adSize: kGADAdSizeBanner)
            addBannerViewToView(bannerView)
            bannerView.adUnitID = "ca-app-pub-3146952223028784/1326xxxx"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
        } else {
            // else, hide the "Remove Ads" button and banner, if it exists
            buttonRemoveAds.isHidden = true
            if bannerView != nil {
                bannerView.removeFromSuperview()
            }
        }
        
        // create a timer that runs triggers a method that checks for active games.
        gameTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(getGameData), userInfo: nil, repeats: true)
        
        DispatchQueue.main.async {
            // Display an activity view notifying the app is checking for games
            self.present(self.activitiyViewController, animated: true, completion: nil)
        }
        
        //check for games immediately
        getGameData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // stop the timer when view is not visible
        gameTimer.invalidate()
    }
    
    // customize the AdMob banner
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    // run an API call to get list of active games on the server
    @objc func getGameData() {
        let parameters: Parameters = [
            "action": "getGames",
            "facebook_id": defaults.string(forKey: "facebook_id")!
        ]
        
        Alamofire.request("http://conecode.com/fives/games.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            // close the activity view
            DispatchQueue.main.async {
                self.activitiyViewController.dismiss(animated: true, completion: nil)
            }
            
            if response.result.value is NSNull {
                return
            }
            
            // parse the JSON response
            let JSON = response.result.value as? NSDictionary
            if let status = JSON?["status"] as? Int {
                if (status == 1) {
                    // assign the response data to an array of dictionaries and sort the array by date the game was created
                    self.games = (JSON?["data"] as! Array<Dictionary<String, Any>>).sorted{
                        (($0 as Dictionary<String, AnyObject>)["added"] as! String) < (($1 as Dictionary<String, AnyObject>)["added"] as! String)
                    }
                    // reload the table viwe
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // Table View methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.games!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gameCell", for: indexPath) as! GameCell
        let imgURLString:String!
        
        // check which player is the current player
        if (defaults.string(forKey: "facebook_id") == self.games![indexPath.row]["player1_facebook_id"] as? String) {
            // if player 1, put in player 2's name and get player 2's Facebook image
            cell.textLabel?.text = self.games![indexPath.row]["player2_name"] as? String
            
            imgURLString = "https://graph.facebook.com/" + (self.games![indexPath.row]["player2_facebook_id"] as? String)! + "/picture?type=normal"
        } else {
            // if player 2, put in player 1's name and get player 1's Facebook image
            cell.textLabel?.text = (self.games![indexPath.row]["player1_name"]! as! String)
            
            imgURLString = "https://graph.facebook.com/" + (self.games![indexPath.row]["player1_facebook_id"] as? String)! + "/picture?type=normal"
        }
        
        // display placeholder image until facebook image loads
        cell.imageView?.sd_setImage(with: URL(string: imgURLString), placeholderImage: UIImage(named: "placeholder.png"))
        
        // check if game is over
        if (self.games![indexPath.row]["winner"] as? String != "0") {
            // if game over, see who won
            if (defaults.string(forKey: "facebook_id") == self.games![indexPath.row]["winner"] as? String) {
                // if current user, highlight green
                cell.backgroundColor = UIColor.green
            } else {
                // if other player, highlight red
                cell.backgroundColor = UIColor.red
            }
        } else if (defaults.string(forKey: "facebook_id") == self.games![indexPath.row]["turn"] as? String) {
            // if game active and it's current player's turn,highlight yellow
            cell.backgroundColor = UIColor.yellow
        } else {
            // else clear
            cell.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    // when row is selected, define game_id and open Play View
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc:PlayViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayViewController") as UIViewController as! PlayViewController
        
        self.defaults.set(self.games![indexPath.row]["game_id"], forKey: "game_id")
        
        self.present(vc, animated: true, completion: nil)
    }
    
    // handle swipe to delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteGame(gameID: (self.games![indexPath.row]["game_id"]! as? String)!)
            self.games.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    // API cal to delete game
    @objc func deleteGame(gameID: String) {
        gameTimer.invalidate()
        let parameters: Parameters = [
            "action": "deleteGame",
            "game_id": gameID
        ]
        
        Alamofire.request("http://conecode.com/fives/games.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            print("Response: \(String(describing: response.result.value))")
            DispatchQueue.main.async {
                self.activitiyViewController.dismiss(animated: true, completion: nil)
            }
            
            if response.result.value is NSNull {
                return
            }
            let JSON = response.result.value as? NSDictionary
            if let status = JSON?["status"] as? Int {
                if (status == 1) {
                }
            }
            
            self.gameTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.getGameData), userInfo: nil, repeats: true)
        }
    }
    
    // open Invite Friend View
    @IBAction func inviteFriend(_ sender: Any) {
        let vc:InviteFriendViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InviteFriendViewController") as UIViewController as! InviteFriendViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    // open Play Friend View
    @IBAction func facebookFriend(_ sender: Any) {
        let vc:FriendsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FriendsViewController") as UIViewController as! FriendsViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    // Open Remove Ads view
    @IBAction func openRemoveAds(_ sender: UIButton) {
        let vc:PurchaseViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PurchaseViewController") as UIViewController as! PurchaseViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    // close the current view
    @IBAction func closeView(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class GameCell: UITableViewCell {
    
}
