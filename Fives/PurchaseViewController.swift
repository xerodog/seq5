//
//  PurchaseViewController.swift
//  Seq5
//  Let's user purchase the ability to remove ads
//
//  Created by Christopher Serra on 2/20/18.
//  Copyright © 2018 Christopher Serra. All rights reserved.
//

import UIKit
import StoreKit

class PurchaseViewController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    @IBOutlet weak var buttonRemoveAds: UIButton!
    let REMOVE_ADS = "remove_ads"
    var productID = ""
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    var removeAdsPurchaseMade = UserDefaults.standard.bool(forKey: "removeAdsPurchaseMade")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonRemoveAds.isEnabled = false;

        fetchAvailableProducts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func removeAdsButton(_ sender: UIButton) {
        print(iapProducts)
        if iapProducts.count > 0 {
            purchaseMyProduct(product: iapProducts[0])
        }
    }
    
    @IBAction func restorePurchaseButt(_ sender: Any) {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        removeAdsPurchaseMade = true
        UserDefaults.standard.set(removeAdsPurchaseMade, forKey: "removeAdsPurchaseMade")
        
        let alert = UIAlertController(title: "Success", message: "You've successfully restored your purchase!", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func fetchAvailableProducts()  {
        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects:
            REMOVE_ADS
        )
        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if (response.products.count > 0) {
            iapProducts = response.products
            
            let remove_ads_prod = response.products[0] as SKProduct
            
            // Get its price from iTunes Connect
            let numberFormatter = NumberFormatter()
            numberFormatter.formatterBehavior = .behavior10_4
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = remove_ads_prod.priceLocale
            let priceStr = numberFormatter.string(from: remove_ads_prod.price)
            
            // Show its description
            buttonRemoveAds.setTitle("Remove Ads (\(priceStr!))", for: .normal)
            buttonRemoveAds.isEnabled = true;
        }
    }
    
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    
    func purchaseMyProduct(product: SKProduct) {
        if self.canMakePurchases() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            
            print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
            productID = product.productIdentifier
        } else {
            let alert = UIAlertController(title: "Error", message: "Purchases are disabled in your device!", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
                    // The Consumable product (remove ads) has been purchased
                    if productID == REMOVE_ADS {
                        // Save your purchase locally (needed only for Non-Consumable IAP)
                        removeAdsPurchaseMade = true
                        UserDefaults.standard.set(removeAdsPurchaseMade, forKey: "removeAdsPurchaseMade")
                        
                        let alert = UIAlertController(title: "Success", message: "Ad removal purchased!", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                            self.closeView("")
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    break
                case .failed:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                case .restored:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                    
                default: break
                }
            }
        }
    }
    
    @IBAction func closeView(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
}
