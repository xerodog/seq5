//
//  HowToPlayViewController.swift
//  Fives
//  Displays how to play
//
//  Created by Christopher Serra on 2/20/18.
//  Copyright © 2018 Christopher Serra. All rights reserved.
//

import UIKit

class HowToPlayViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        let containerViewBounds = contentView.bounds
        
        scrollView.contentSize = CGSize(width: containerViewBounds.size.width, height: 1040)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeView(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }

}
