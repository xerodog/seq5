//
//  PlayViewController.swift
//  Seq5
//  Runs the actual game play
//
//  Created by Christopher Serra on 12/22/17.
//  Copyright © 2017 Christopher Serra. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMobileAds

class PlayViewController: UIViewController,GADInterstitialDelegate {
    var cards:[[String:Any]]? = []
    var positionsPlayed1:[Int] = []
    var positionsPlayed2:[Int] = []
    var positionsLocked:[Int] = []
    var newLocked:[Int] = []
    var myHand:[[String: Any]]? = []
    var selectedCard:Int = 0
    var selectedCardName:String = ""
    var selectedCardId:String = ""
    var movePosition:Int = 0
    var moveAdd:Int = 1
    var locksFound:Int = 0
    var positions:[[String:Any]]? = []
    
    let defaults = UserDefaults.standard
    var lastCard:[String:Any] = [:]
    var gameTimer: Timer!
    var boardLaidOut: Bool! = false
    var gamePaused: Bool! = false
    var showedWin: Bool! = false
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    @IBOutlet weak var view9: UIView!
    @IBOutlet weak var view10: UIView!
    @IBOutlet weak var player1Name: UILabel!
    @IBOutlet weak var player2Name: UILabel!
    @IBOutlet weak var player1Color: UIView!
    @IBOutlet weak var player2Color: UIView!
    @IBOutlet weak var player1Image: UIImageView!
    @IBOutlet weak var player2Image: UIImageView!
    @IBOutlet weak var player1Number: UILabel!
    @IBOutlet weak var player1Suit: UIImageView!
    @IBOutlet weak var player2Suit: UIImageView!
    @IBOutlet weak var player2Number: UILabel!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var labelLoading: UILabel!
    var interstitial: GADInterstitial!
    @IBOutlet weak var buttonRemoveAds: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pauseGame(pause:false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        // check if they purchased the remove ads IAP
        if !defaults.bool(forKey: "removeAdsPurchaseMade") {
            interstitial = createAndLoadInterstitial()
        } else {
            buttonRemoveAds.isHidden = true
        }
        self.labelLoading.text = "Loading Game..."
        self.viewLoading.isHidden = false
        self.getGameData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // diable game timer
        gameTimer.invalidate()
    }
    
    // loads the full page ad into memory
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3146952223028784/7864xxxx")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    // closes full page ad
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        // restart game timer
        gameTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(checkTurn), userInfo: nil, repeats: true)
        interstitial = createAndLoadInterstitial()
        self.getGameData()
    }
    
    @objc func pauseGame(pause:Bool) {
        if pause {
            // stops game timer
            gameTimer.invalidate()
            gamePaused = true
        } else {
            // restats game timer
            gameTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(checkTurn), userInfo: nil, repeats: true)
            gamePaused = false
        }
    }
    
    // check the server to see whose turn it is
    @objc func checkTurn() {
        let parameters: Parameters = [
            "action": "checkTurn",
            "game_id": defaults.string(forKey: "game_id")!
        ]
        
        Alamofire.request("http://conecode.com/fives/games.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
//            print(response)
            if response.result.value is NSNull {
                return
            }
            
            let JSON = response.result.value as? NSDictionary
            let keyExists = JSON?["status"] != nil
            if keyExists {
                let status = JSON?["status"] as! String
                if (status == self.defaults.string(forKey: "facebook_id")) {
                    self.pauseGame(pause:true)
                    self.getGameData()
                }
            }
        }
    }
    
    // retrieve game data from server
    @objc func getGameData() {
        let parameters: Parameters = [
            "action": "getGame",
            "game_id": defaults.string(forKey: "game_id")!
        ]
        
        Alamofire.request("http://conecode.com/fives/games.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            self.viewLoading.isHidden = true
            
            if response.result.value is NSNull {
                return
            }
            
            self.positionsLocked = []
            self.positionsPlayed1 = []
            self.positionsPlayed2 = []
            self.myHand = []
            
            let JSON = response.result.value as? NSDictionary
            let keyExists = JSON?["status"] != nil
            if keyExists {
                let status = JSON?["status"] as! Int
                
                if (status == 1) {
                    // define game cards
                    self.cards = (JSON?["data"] as! Array<Dictionary<String, Any>>).sorted {
                        (($0 as Dictionary<String, AnyObject>)["play_date"] as! String) < (($1 as Dictionary<String, AnyObject>)["play_date"] as! String)
                    }
                    
                    if (JSON?["positions"] != nil) {
                        // set the gameboard data
                        self.positions = (JSON?["positions"] as! Array<Dictionary<String, Any>>)
                        
                        // the the board has not been laid out already, do so
                        if (!self.boardLaidOut) {
                            self.layoutGameboard()
                            self.boardLaidOut = true
                        }
                        
                        // Get last card played for information purposes
                        if self.cards!.count > 0 {
                            self.lastCard = self.cards![self.cards!.count - 1]
                        } else {
                            self.lastCard = [:]
                        }
                        
                        // loop through all the card in the deck
                        for card:Dictionary<String, Any> in self.cards! {
                            // assign cards to hand
                            if (card["dealt_to"] as! String != "0" && card["played"] as? String == "0") {
                                if (card["dealt_to"] as? String == self.defaults.string(forKey: "facebook_id")) {
                                    self.myHand?.append(card)
                                    
                                }
                            }
                            let played_on = (card["played_on"] as! NSString).doubleValue
                            
                            // if card is locked, set the lock image view image
                            if let imageLock = self.view.viewWithTag(Int(300+played_on)) as? UIImageView {
                                if (card["locked"] as? String == "1") {
                                    imageLock.image = UIImage(named: "lock")
                                } else {
                                    imageLock.image = nil
                                }
                            }
                            
                            // if card has been played
                            if (card["played"] as? String == "1") {
                                // remove any lock buttons that may exist
                                if let theButton1 = self.view.viewWithTag(Int(900+played_on)) as? UIButton {
                                    theButton1.removeFromSuperview()
                                }
                                
                                // define position button
                                if let theButton = self.view.viewWithTag(Int(played_on)) as? UIButton {
                                    // if button was played by player 1, highlight the button blue, and add it to an array of their played cards
                                    if (card["dealt_to"] as? String == card["player1_facebook_id"] as? String) {
                                        if card["card"] as! String != "jc" && card["card"] as! String != "jh" {
                                            theButton.backgroundColor = .blue
                                            self.positionsPlayed1.append(Int(played_on))
                                        } else {
                                            theButton.backgroundColor = .clear
                                        }
                                    } else {
                                        // if button was played by player 2, highlight the button yellow, and add it to an array of their played cards
                                        if card["card"] as! String != "jc" && card["card"] as! String != "jh" {
                                            theButton.backgroundColor = .yellow
                                            self.positionsPlayed2.append(Int(played_on))
                                        } else {
                                            theButton.backgroundColor = .clear
                                        }
                                    }
                                    
                                    // if position is locked, disable the button, and add it to an array of locked positions
                                    if (card["locked"] as? String == "1") {
                                        theButton.isEnabled = false
                                        self.positionsLocked.append(Int(played_on))
                                    }
                                }
                            }
                        }
                        
                        // use the last card data to fill in all the game data (player names, image, and las card played info) Also checks the winner
                        if self.lastCard.count > 0 {
                            self.player1Name.text = (self.lastCard["player1_name"] as! String)
                            self.player1Image?.sd_setImage(with: URL(string: "https://graph.facebook.com/" + (self.lastCard["player1_facebook_id"] as! String) + "/picture?type=normal"), placeholderImage: UIImage(named: "placeholder.png"))
                            
                            self.player2Name.text = (self.lastCard["player2_name"] as! String)
                            self.player2Image?.sd_setImage(with: URL(string: "https://graph.facebook.com/" + (self.lastCard["player2_facebook_id"] as! String) + "/picture?type=normal"), placeholderImage: UIImage(named: "placeholder.png"))
                            
//                            if (self.lastCard["played"] as! String != "0") {
//                                let played_on = (self.lastCard["played_on"] as! NSString).doubleValue
//
//                                if (played_on != 0) {
//                                    if let theButton = self.view.viewWithTag(Int(played_on)) as? UIButton {
//                                        if self.lastCard["card"] as! String != "jc" && self.lastCard["card"] as! String != "jh" {
//                                            if (self.lastCard["dealt_to"] as! String == self.lastCard["player1_facebook_id"] as! String) {
//                                                theButton.backgroundColor = .blue
//                                            } else {
//                                                theButton.backgroundColor = .yellow
//                                            }
//                                        }
//                                    }
//                                }
//                            }
                            let card:[String:Any] = self.lastCard
                            let cardName:String = card["card"] as! String
                            let suit = String(describing: cardName.last!)
                            let number = cardName.dropLast()
                            
                            if (self.lastCard["played"] as! String == "1") {
                                if (self.lastCard["dealt_to"] as! String == self.lastCard["player1_facebook_id"] as! String) {
                                    self.player1Suit.image = UIImage(named:suit)
                                    self.player1Number.text = String(describing: number).uppercased()
                                    self.player2Suit.image = nil
                                    self.player2Number.text = ""
                                } else {
                                    self.player2Suit.image = UIImage(named:suit)
                                    self.player2Number.text = String(describing: number).uppercased()
                                    self.player1Suit.image = nil
                                    self.player1Number.text = ""
                                }
                            }
                            
                            // if game over, tell use they won or loss
                            if self.lastCard["winner"] as? String != "0" && !self.showedWin {
                                self.showedWin = true
                                self.gameTimer.invalidate()
                                
                                var winnerString:String = ""
                                if self.lastCard["winner"] as? String == self.defaults.string(forKey: "facebook_id") {
                                    winnerString = "You Win!"
                                    self.labelStatus.text = "You Win!"
                                } else {
                                    winnerString = "You Lost!"
                                    self.labelStatus.text = "You Lost!"
                                }
                                let alert = UIAlertController(title: "Game Over", message: winnerString, preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: { action in
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                                for i in 1...7 {
                                    if let theHandButton = self.view.viewWithTag(i+100) as? UIButton {
                                        theHandButton.isEnabled = false
                                        theHandButton.backgroundColor = .black
                                    }
                                }
                            } else {
                                // tell user who's turn it is
                                if ((self.lastCard["turn"] as! String) == self.defaults.string(forKey: "facebook_id")) {
                                    self.pauseGame(pause:true)
                                    for i in 1...7 {
                                        if let theHandButton = self.view.viewWithTag(i+100) as? UIButton {
                                            theHandButton.isEnabled = true
                                            theHandButton.backgroundColor = .clear
                                        }
                                    }
                                }
                                
                                // highlight the name of the player who's turn it is
                                if (self.lastCard["turn"] as! String == self.lastCard["player1_facebook_id"] as! String) {
                                    self.player1Name.backgroundColor = .white
                                    self.player1Name.textColor = .black
                                    self.player2Name.backgroundColor = .clear
                                    self.player2Name.textColor = .white
                                    self.labelStatus.text = (self.lastCard["player1_name"] as! String)+"'s turn"
                                } else {
                                    self.player2Name.backgroundColor = .white
                                    self.player2Name.textColor = .black
                                    self.player1Name.backgroundColor = .clear
                                    self.player1Name.textColor = .white
                                    self.labelStatus.text = (self.lastCard["player2_name"] as! String)+"'s turn"
                                }
                                
                                // If the card was dealt, but not played, put it in the players hand
                                for i in 1...7 {
                                    if let theCard = self.view.viewWithTag(200+i) as? UIImageView {
                                        theCard.image = UIImage(named:"blank_card")
                                    }
                                    
                                    let card:[String:Any] = self.myHand![i-1]
                                    let cardName:String = card["card"] as! String
                                    let suit = String(describing: cardName.last!)
                                    
                                    if let theSuit = self.view.viewWithTag(700+i) as? UIImageView {
                                        theSuit.image = UIImage(named:suit)
                                    }
                                    
                                    var number = cardName.dropLast()
                                    if number == "j" {
                                        if suit == "h" || suit == "c" {
                                            number = "j-"
                                        } else {
                                            number = "j+"
                                        }
                                    }
                                    
                                    if let theNumber = self.view.viewWithTag(800+i) as? UILabel {
                                        theNumber.text = String(describing: number).uppercased()
                                    }
                                }
                            }
                        }
                    } else {
                        self.closeView("")
                    }
                } else {
                    let message = JSON?["message"] as! String
                    print(message)
                }
            }
        }
    }
    
    // sorts data
    func dateSort(p1:[String:String], p2:[String:String]) -> Bool {
        guard let s1 = p1["surname"], let s2 = p2["surname"] else {
            return false
        }
        
        if s1 == s2 {
            guard let g1 = p1["given"], let g2 = p2["given"] else {
                return false
            }
            return g1 < g2
        }
        
        return s1 < s2
    }
    
    // when card in player's hand is selected
    @IBAction func handCardSelected(_ sender: UIButton) {
        let tag: Int = sender.tag - 100
        self.selectedCard = tag
        self.selectedCardName = self.myHand![tag-1]["card"] as! String
        self.selectedCardId = self.myHand![tag-1]["card_id"] as! String
        
        for i in 1...7 {
            if let theCardButton = self.view.viewWithTag(i+100) as? UIButton {
                theCardButton.backgroundColor = .clear
            }
        }
        
        if sender.backgroundColor == .green {
            // if card is already selected, un-highlight positions on the board
            sender.backgroundColor = .clear
            for position_item:Dictionary<String, Any> in self.positions! {
                let pos:Int = position_item["position"] as! Int
                if let theCardButton = self.view.viewWithTag(pos) as? UIButton {
                    if theCardButton.backgroundColor == .green {
                        theCardButton.backgroundColor = .clear
                    } else if theCardButton.backgroundColor == .magenta || theCardButton.backgroundColor == .red {
                        if self.positionsPlayed1.contains(pos) {
                            theCardButton.backgroundColor = .blue
                        } else if self.positionsPlayed2.contains(pos) {
                            theCardButton.backgroundColor = .yellow
                        }
                    }
                }
            }
        } else {
            // highlight positions on the board that match selected card
            sender.backgroundColor = .green
            let index = self.selectedCardName.index(self.selectedCardName.startIndex, offsetBy: 0)
            var spotsAvail:Int = 0;
            for position_item:Dictionary<String, Any> in self.positions! {
                let pos:Int = Int(position_item["position"] as! String)!
                let card:String = position_item["card"] as! String
                if let theCardButton = self.view.viewWithTag(pos) as? UIButton {
                    if (self.selectedCardName[index] == "j") {
                        // if the selected card is a jack
                        if self.selectedCardName == "js" || self.selectedCardName == "jd" {
                            // if it's a plus jack, highlight all available positions on the board
                            if !self.positionsPlayed1.contains(pos) && !self.positionsPlayed2.contains(pos) {
                                if pos != 1 && pos != 10 && pos != 91 && pos != 100 {
                                    theCardButton.backgroundColor = .green
                                    spotsAvail = spotsAvail + 1
                                }
                            } else {
                                if self.positionsPlayed1.contains(pos) {
                                    theCardButton.backgroundColor = .blue
                                } else if self.positionsPlayed2.contains(pos) {
                                    theCardButton.backgroundColor = .yellow
                                }
                            }
                        } else if self.selectedCardName == "jh" || self.selectedCardName == "jc" {
                            // if it's a minus jack, highlight all opponents positions on the board that are not locked
                            if !self.positionsLocked.contains(pos) {
                                if (self.lastCard["player1_facebook_id"] as? String == self.defaults.string(forKey: "facebook_id")) {
                                    if self.positionsPlayed2.contains(pos) {
                                        theCardButton.backgroundColor = .red
                                        spotsAvail = spotsAvail + 1
                                    } else {
                                        if !self.positionsPlayed1.contains(pos) {
                                            theCardButton.backgroundColor = .clear
                                        }
                                    }
                                } else {
                                    if self.positionsPlayed1.contains(pos) {
                                        theCardButton.backgroundColor = .red
                                        spotsAvail = spotsAvail + 1
                                    } else {
                                        if !self.positionsPlayed2.contains(pos) {
                                            theCardButton.backgroundColor = .clear
                                        }
                                    }
                                }
                            }
                        } else {
                            if theCardButton.backgroundColor == .green {
                                theCardButton.backgroundColor = .clear
                            } else if theCardButton.backgroundColor == .magenta || theCardButton.backgroundColor == .red {
                                if self.positionsPlayed1.contains(pos) {
                                    theCardButton.backgroundColor = .blue
                                } else if self.positionsPlayed2.contains(pos) {
                                    theCardButton.backgroundColor = .yellow
                                }
                            }
                        }
                    } else if card == self.selectedCardName {
                        // else highlight matching spots green
                        if !self.positionsPlayed1.contains(pos) && !self.positionsPlayed2.contains(pos) {
                            if pos != 1 && pos != 10 && pos != 91 && pos != 100 {
                                theCardButton.backgroundColor = .green
                                spotsAvail = spotsAvail + 1
                            } else {
                                theCardButton.backgroundColor = .clear
                            }
                        } else {
                            // if position is already taken, highlight it magenta
                            theCardButton.backgroundColor = .magenta
                        }
                    } else {
                        // else change colors back to their original state
                        if theCardButton.backgroundColor == .green {
                            theCardButton.backgroundColor = .clear
                        } else if theCardButton.backgroundColor == .magenta || theCardButton.backgroundColor == .red {
                            if self.positionsPlayed1.contains(pos) {
                                theCardButton.backgroundColor = .blue
                            } else if self.positionsPlayed2.contains(pos) {
                                theCardButton.backgroundColor = .yellow
                            }
                        }
                    }
                }
            }
            
            if spotsAvail == 0 {
                // if there are no spots available, alert user
                let alert = UIAlertController(title: "Discard", message: "There are no spots available for this card. Would you like to discard it for a new card?", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                    self.movePosition = 0
                    self.sendMove()
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // when spot on board is selected
    @IBAction func placeChip(_ sender: UIButton) {
        movePosition = sender.tag
        if sender.backgroundColor == .green {
            // add card to positions played arrays
            if let theCardButton = self.view.viewWithTag(movePosition) as? UIButton {
                if (self.lastCard["player1_facebook_id"] as? String == self.defaults.string(forKey: "facebook_id")) {
                    theCardButton.backgroundColor = .blue
                    self.positionsPlayed1.append(Int(movePosition))
                } else {
                    theCardButton.backgroundColor = .yellow
                    self.positionsPlayed2.append(Int(movePosition))
                }
            }
            
            // disable card button in the hand
            for i in 1...7 {
                if let theCardButton = self.view.viewWithTag(i+100) as? UIButton {
                    theCardButton.backgroundColor = UIColor.darkGray
                    theCardButton.isEnabled = false
                }
            }
            
            for i in 2...99 {
                if i != 10 && i != 91 {
                    // make all other cards that haven't been played clear
                    if !self.positionsPlayed1.contains(i) && !self.positionsPlayed2.contains(i) {
                        if let theCardButton = self.view.viewWithTag(i) as? UIButton {
                            theCardButton.backgroundColor = UIColor.clear
                        }
                    }
                }
            }
            
            // change selected hand card to a card back
            if let theHandCard = self.view.viewWithTag(Int(200+self.selectedCard)) as? UIImageView {
                theHandCard.image = UIImage(named:"card_back")
            }
            
            // replace the suit and number to nil
            if let theSuit = self.view.viewWithTag(Int(700+self.selectedCard)) as? UIImageView {
                theSuit.image = nil
            }
            if let theNumber = self.view.viewWithTag(Int(800+self.selectedCard)) as? UILabel {
                theNumber.text = ""
            }
            
            // run method that checks for sequences of 5
            self.check5()
        } else if sender.backgroundColor == .red {
            // if card is red, remove card from positions played
            moveAdd = 0
            if self.positionsPlayed1.contains(movePosition) {
                let index: Int = self.positionsPlayed1.index(of: movePosition)!
                self.positionsPlayed1.remove(at: index)
            } else if self.positionsPlayed2.contains(movePosition) {
                let index: Int = self.positionsPlayed2.index(of: movePosition)!
                self.positionsPlayed2.remove(at: index)
            }
            
            for i in 1...7 {
                if let theCardButton = self.view.viewWithTag(i+100) as? UIButton {
                    theCardButton.backgroundColor = UIColor.darkGray
                    theCardButton.isEnabled = false
                }
            }
            
            for i in 2...99 {
                if i != 10 && i != 91 {
                    if !self.positionsPlayed1.contains(i) && !self.positionsPlayed2.contains(i) {
                        if let theCardButton = self.view.viewWithTag(i) as? UIButton {
                            theCardButton.backgroundColor = UIColor.clear
                        }
                    }
                }
            }
            
            if let theHandCard = self.view.viewWithTag(Int(200+self.selectedCard)) as? UIImageView {
                theHandCard.image = UIImage(named:"card_back")
            }
            self.sendMove()
        } else {
            self.labelStatus.text = "Cannot select this spot"
        }
    }
    
    // start check for sequence of 5
    func check5() {
        var processed:Bool = false
        var positionsFound:Array<Int> = []
        var searchArray:Array<Int> = []
        let positionWilds:Array<Int> = [1,10,91]
        let positionIncrements:Array<Int> = [1,9,10,11]
        if (self.lastCard["player1_facebook_id"] as? String == self.defaults.string(forKey: "facebook_id")) {
            searchArray = self.positionsPlayed1.sorted()
            searchArray.append(contentsOf: positionWilds)
            for pos in searchArray {
                for posInc in positionIncrements {
                    positionsFound = checkPositions(intStart: pos, arrPlayed: searchArray, increment: posInc)
                    if (positionsFound.count > 4) {
                        processFive(positionsFound:positionsFound)
                        processed = true
                        break
                    }
                }
                if (positionsFound.count > 4) {
                    print(positionsFound)
                    break
                }
            }
        } else {
            searchArray = self.positionsPlayed2.sorted()
            searchArray.append(contentsOf: positionWilds)
            for pos in searchArray {
                for posInc in positionIncrements {
                    positionsFound = checkPositions(intStart: pos, arrPlayed: searchArray, increment: posInc)
                    if (positionsFound.count > 4) {
                        processFive(positionsFound:positionsFound)
                        processed = true
                        break
                    }
                }
                if (positionsFound.count > 4) {
                    print(positionsFound)
                    break
                }
            }
        }
        if (!processed) {
            sendMove()
        }
    }
    
    // calculate sequences of 5
    func checkPositions(intStart:Int, arrPlayed:Array<Int>, increment:Int) -> Array<Int> {
        var positionsFound:Array<Int> = []
        var lockedFound:Int = 0
        positionsFound.append(intStart)
        for i in 1...9 {
            let intStep:Int = (increment*i)
            if (arrPlayed.contains(intStart+intStep) || intStart+intStep == 10 || intStart+intStep == 91 || intStart+intStep == 100 || intStart+intStep == 1) {
                if ((positionsLocked.contains(intStart+intStep)) && lockedFound == 0) || !(positionsLocked.contains(intStart+intStep)) || intStart+intStep == 10 || intStart+intStep == 91 || intStart+intStep == 100 || intStart+intStep == 1 {
                    if (positionsLocked.contains(intStart+intStep)) {
                        lockedFound = lockedFound + 1
                    }
                    positionsFound.append(intStart+intStep)
//                    print(String(describing: positionsFound)+" - "+String(describing: increment))
                    if (increment == 1 || increment == 11) {
                        if intStart+intStep == 10 || intStart+intStep == 20 || intStart+intStep == 30 || intStart+intStep == 40 || intStart+intStep == 50 || intStart+intStep == 60 || intStart+intStep == 70 || intStart+intStep == 80 || intStart+intStep == 90 || intStart+intStep == 100  {
                            return positionsFound
                        }
                    } else if (increment == 9) {
                        print(intStart)
                        print(positionsFound)
                        if intStart+intStep == 11 || intStart == 1 || intStart+intStep == 21 || intStart+intStep == 31 || intStart+intStep == 41 || intStart+intStep == 51 || intStart+intStep == 61 || intStart+intStep == 71 || intStart+intStep == 81 || intStart+intStep == 91 || intStart == 11 || intStart == 21 || intStart == 31 || intStart == 41 || intStart == 51 || intStart == 61 || intStart == 71 || intStart == 81 || intStart == 91 {
                            return positionsFound
                        }
                    } else if (increment == 10) {
                        if intStart+intStep > 90 || intStart < 50 {
                            return positionsFound
                        }
                    }
                }
            } else {
                return positionsFound
            }
        }
        return positionsFound
    }
    
    // determine what to do with a found sequence
    func processFive(positionsFound:Array<Int>) {
        if positionsFound.count == 5 || positionsFound.count == 9 {
            self.newLocked = positionsFound
            
            self.positionsLocked.append(contentsOf: self.newLocked)
            
            self.sendLocks(locks: self.newLocked)
        } else {
            selectCardsToLock(potentialLocks:positionsFound)
        }
    }
    
    // if more than 5 were found in a sequence, prompt user to select which ones to lock
    func selectCardsToLock(potentialLocks:Array<Int>) {
        self.labelStatus.text = "Select 5 spots..."
        self.newLocked = []
        for i in 1...100 {
            if potentialLocks.contains(i) {
                var x:Int = 0
                if (i < 11) {
                    x = i-1
                } else if (i < 21) {
                    x = i-11
                } else if (i < 31) {
                    x = i-21
                } else if (i < 41) {
                    x = i-31
                } else if (i < 51) {
                    x = i-41
                } else if (i < 61) {
                    x = i-51
                } else if (i < 71) {
                    x = i-61
                } else if (i < 81) {
                    x = i-71
                } else if (i < 91) {
                    x = i-81
                } else {
                    x = i-91
                }
                let button = UIButton(frame:CGRect(x:(x * 36), y:0, width:34, height:34))
                button.backgroundColor = UIColor.cyan
                button.alpha = 0.3
                button.tag = 900+i
                button.setTitle("",for: .normal)
                button.addTarget(self, action: #selector(lockPosition), for: .touchUpInside)
                
                if (i < 11) {
                    view1.addSubview(button)
                } else if (i < 21) {
                    view2.addSubview(button)
                } else if (i < 31) {
                    view3.addSubview(button)
                } else if (i < 41) {
                    view4.addSubview(button)
                } else if (i < 51) {
                    view5.addSubview(button)
                } else if (i < 61) {
                    view6.addSubview(button)
                } else if (i < 71) {
                    view7.addSubview(button)
                } else if (i < 81) {
                    view8.addSubview(button)
                } else if (i < 91) {
                    view9.addSubview(button)
                } else {
                    view10.addSubview(button)
                }
            }
        }
    }
    
    // lock positions if they are sequanced.
    @objc func lockPosition(sender:UIButton) {
        let position:Int = sender.tag - 900
        if sender.backgroundColor == .cyan {
            sender.backgroundColor = .green
            self.newLocked.append(position)
        } else {
            sender.backgroundColor = .cyan
            self.newLocked = self.newLocked.filter { $0 != position }
        }
        if self.newLocked.count == 5 {
            self.newLocked = self.newLocked.sorted()
            if (self.newLocked[4] - self.newLocked[3]) == (self.newLocked[3] - self.newLocked[2]) && (self.newLocked[3] - self.newLocked[2]) == (self.newLocked[2] - self.newLocked[1]) && (self.newLocked[2] - self.newLocked[1]) == (self.newLocked[1] - self.newLocked[0]) {
                let alert = UIAlertController(title: "Lock", message: "Are these the spots you want?", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                    self.sendLocks(locks: self.newLocked)
                    for pos in self.newLocked {
                        if let theButton = self.view.viewWithTag(Int(pos+900)) as? UIButton {
                            theButton.backgroundColor = .cyan
                        }
                    }
                    self.newLocked = []
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: { action in
                    for pos in self.newLocked {
                        if let theButton = self.view.viewWithTag(Int(pos+900)) as? UIButton {
                            theButton.backgroundColor = .cyan
                        }
                    }
                    self.newLocked = []
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // send move to the server
    func sendMove() {
        self.labelLoading.text = "Sending Move..."
        self.viewLoading.isHidden = false
        let parameters: Parameters = [
            "action": "move",
            "game_id": defaults.string(forKey: "game_id")!,
            "player": self.defaults.string(forKey: "facebook_id")!,
            "card_id": self.selectedCardId,
            "position": movePosition,
            "add": moveAdd
        ]
//        print(parameters)
        
        Alamofire.request("http://conecode.com/fives/games.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if let error = response.result.error {
                print(error)
                self.getGameData()
            } else {
                if self.newLocked.count > 4 {
                    self.sendLocks(locks: self.newLocked)
                } else {
                    self.pauseGame(pause:false)
                    self.getGameData()
                    self.viewLoading.isHidden = true
                    if !self.defaults.bool(forKey: "removeAdsPurchaseMade") {
                        if self.interstitial.isReady {
                            self.interstitial.present(fromRootViewController: self)
                        } else {
                            self.interstitial = self.createAndLoadInterstitial()
                            print("Ad wasn't ready")
                        }
                    }
                }
            }
        }
    }
    
    // send locks to the server
    func sendLocks(locks: Array<Int>) {
        if locks.count > 3 {
            let parameters: Parameters = [
                "action": "locks",
                "game_id": defaults.string(forKey: "game_id")!,
                "player": self.defaults.string(forKey: "facebook_id")!,
                "card_id": self.selectedCardId,
                "position": movePosition,
                "add": moveAdd,
                "locks": locks
            ]
            self.newLocked = []
            Alamofire.request("http://conecode.com/fives/games.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
                if let error = response.result.error {
                    print(error)
                    self.getGameData()
                } else {
//                    print("RESPONSE: \(String(describing: response.result.value))")
                    self.viewLoading.isHidden = true
                    self.pauseGame(pause:false)
                    self.getGameData()
                }
            }
        }
    }
    
    // layout the gameboard
    func layoutGameboard() {
        var pos: Int
        for the_position:Dictionary<String, Any> in self.positions! {
            pos = Int((the_position["position"] as! String))!
            let card:String = the_position["card"] as! String
            var imageCard : UIImageView
            var imageSuit : UIImageView
            var labelNumber : UILabel
            var imageLock : UIImageView
            var buttonPosition : UIButton
            var x:Int = 0
            if (pos < 11) {
                x = pos-1
            } else if (pos < 21) {
                x = pos-11
            } else if (pos < 31) {
                x = pos-21
            } else if (pos < 41) {
                x = pos-31
            } else if (pos < 51) {
                x = pos-41
            } else if (pos < 61) {
                x = pos-51
            } else if (pos < 71) {
                x = pos-61
            } else if (pos < 81) {
                x = pos-71
            } else if (pos < 91) {
                x = pos-81
            } else {
                x = pos-91
            }
            
            imageCard  = UIImageView(frame:CGRect(x:(x * 32), y:0, width:32, height:32));
            imageSuit  = UIImageView(frame:CGRect(x:((x * 32) + 9), y:9, width:23, height:23));
            labelNumber  = UILabel(frame:CGRect(x:(x * 32), y:0, width:23, height:23));
            imageLock  = UIImageView(frame:CGRect(x:(x * 32), y:0, width:32, height:32));
            buttonPosition  = UIButton(frame:CGRect(x:(x * 32), y:0, width:32, height:32));
            labelNumber.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
            
            buttonPosition.alpha = 0.3
            buttonPosition.tag = pos
            imageLock.tag = 300+pos
            buttonPosition.addTarget(self, action: #selector(placeChip), for: .touchUpInside)
            
            labelNumber.textAlignment = .center
            labelNumber.textColor = UIColor.white
            
            let suit = String(describing: card.last!)
            
            imageSuit.image = UIImage(named:suit)
            let number = card.dropLast()
            labelNumber.text = String(describing: number).uppercased()
            
            imageCard.image = UIImage(named:"blank_card")
            if (pos < 11) {
                view1.addSubview(imageCard)
                view1.addSubview(imageSuit)
                view1.addSubview(labelNumber)
                view1.addSubview(imageLock)
                view1.addSubview(buttonPosition)
            } else if (pos < 21) {
                view2.addSubview(imageCard)
                view2.addSubview(imageSuit)
                view2.addSubview(labelNumber)
                view2.addSubview(imageLock)
                view2.addSubview(buttonPosition)
            } else if (pos < 31) {
                view3.addSubview(imageCard)
                view3.addSubview(imageSuit)
                view3.addSubview(labelNumber)
                view3.addSubview(imageLock)
                view3.addSubview(buttonPosition)
            } else if (pos < 41) {
                view4.addSubview(imageCard)
                view4.addSubview(imageSuit)
                view4.addSubview(labelNumber)
                view4.addSubview(imageLock)
                view4.addSubview(buttonPosition)
            } else if (pos < 51) {
                view5.addSubview(imageCard)
                view5.addSubview(imageSuit)
                view5.addSubview(labelNumber)
                view5.addSubview(imageLock)
                view5.addSubview(buttonPosition)
            } else if (pos < 61) {
                view6.addSubview(imageCard)
                view6.addSubview(imageSuit)
                view6.addSubview(labelNumber)
                view6.addSubview(imageLock)
                view6.addSubview(buttonPosition)
            } else if (pos < 71) {
                view7.addSubview(imageCard)
                view7.addSubview(imageSuit)
                view7.addSubview(labelNumber)
                view7.addSubview(imageLock)
                view7.addSubview(buttonPosition)
            } else if (pos < 81) {
                view8.addSubview(imageCard)
                view8.addSubview(imageSuit)
                view8.addSubview(labelNumber)
                view8.addSubview(imageLock)
                view8.addSubview(buttonPosition)
            } else if (pos < 91) {
                view9.addSubview(imageCard)
                view9.addSubview(imageSuit)
                view9.addSubview(labelNumber)
                view9.addSubview(imageLock)
                view9.addSubview(buttonPosition)
            } else {
                view10.addSubview(imageCard)
                view10.addSubview(imageSuit)
                view10.addSubview(labelNumber)
                view10.addSubview(imageLock)
                view10.addSubview(buttonPosition)
            }
        }
    }
    
    // Open Remove Ads view
    @IBAction func openRemoveAds(_ sender: UIButton) {
        let vc:PurchaseViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PurchaseViewController") as UIViewController as! PurchaseViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    // close the current view
    @IBAction func closeView(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
