//
//  FriendsViewController.swift
//  Seq5
//  Let's user select a Facebook Friend that have installed the app to create a game
//
//  Created by Christopher Serra on 12/23/17.
//  Copyright © 2017 Christopher Serra. All rights reserved.
//

import UIKit
import FacebookCore
import SDWebImage
import Alamofire

class FriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    let defaults = UserDefaults.standard
    var friends:Array<Dictionary<String, String>>? = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // get list of facebook friends that have installed the app
        let graphRequest = GraphRequest(graphPath: "me/friends")
        graphRequest.start { (urlResponse, requestResult) in
            switch requestResult {
            case .failed(let error):
                print("error in graph request:", error)
                break
                // Handle the result's error
                
            case .success(let graphResponse):
                if let responseDictionary = graphResponse.dictionaryValue {
                    self.friends = responseDictionary["data"] as? Array<Dictionary<String, String>>
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // Table View methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friends!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "friendCell", for: indexPath)
        
        cell.textLabel?.text = self.friends![indexPath.row]["name"]!
        
        // Get the friend's Facebook image
        let imgURLString = "https://graph.facebook.com/" + self.friends![indexPath.row]["id"]! + "/picture?type=normal"
        cell.imageView?.sd_setImage(with: URL(string: imgURLString), placeholderImage: UIImage(named: "placeholder.png"))

        return cell
    }
    
    // when row is selected, send API call to server to create the game specifications
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // load activity
        let activitiyViewController = ActivityViewController(message: "Creating...", hideIndicator: false)
        present(activitiyViewController, animated: true, completion: nil)
        
        var email = ""
        if (self.friends![indexPath.row]["email"] != nil) {
            email = self.friends![indexPath.row]["email"]!
        }
        
        let parameters: Parameters = [
            "action": "new",
            "player1_name": defaults.string(forKey: "name")!,
            "player1_facebook_id": defaults.string(forKey: "facebook_id")!,
            "player2_name": self.friends![indexPath.row]["name"]!,
            "player2_email": email,
            "player2_facebook_id": self.friends![indexPath.row]["id"]!
        ]
        
        Alamofire.request("http://conecode.com/fives/games.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
//            print("Response: \(String(describing: response.result.value))")
            activitiyViewController.dismiss(animated: true, completion: {
                self.closeView(response)
            })
            
            if response.result.value is NSNull {
                return
            }
        }
    }
    
    // close the current view
    @IBAction func closeView(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class FriendsCell: UITableViewCell {
}
