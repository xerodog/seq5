//
//  ViewController.swift
//  Seq5
// Login view
//
//  Created by Christopher Serra on 12/19/17.
//  Copyright © 2017 Christopher Serra. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import Alamofire
import GoogleMobileAds

class ViewController: UIViewController {
    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var buttonHowTo: UIButton!
    @IBOutlet weak var buttonFacebook: UIButton!
    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var labelConnect: UILabel!
    let defaults = UserDefaults.standard
    var bannerView: GADBannerView!
    @IBOutlet weak var buttonRemoveAds: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // check if they purchased the remove ads IAP
        if !defaults.bool(forKey: "removeAdsPurchaseMade") {
            // if not, create the AdMob advertisement banner
            bannerView = GADBannerView(adSize: kGADAdSizeBanner)
            addBannerViewToView(bannerView)
            bannerView.adUnitID = "ca-app-pub-3146952223028784/13264xxxx"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
        } else {
            // else, hide the "Remove Ads" button and banner, if it exists
            buttonRemoveAds.isHidden = true
            if bannerView != nil {
                bannerView.removeFromSuperview()
            }
        }
        
        // check if they are logged into facebook
        if (defaults.bool(forKey: "facebook") && (defaults.string(forKey: "facebook_id") != nil)) {
            // if they are, display and hide the necessary buttons and labels
            self.buttonFacebook.isHidden = true
            self.buttonLogout.isHidden = false
            self.buttonPlay.isHidden = false
            self.labelConnect.isHidden = true
        } else {
            // if not, display and hide the necessary buttons and labels
            self.buttonFacebook.isHidden = false
            self.buttonLogout.isHidden = true
            self.buttonPlay.isHidden = true
            self.labelConnect.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // customize the AdMob banner
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    // Open FB Login
    @IBAction func openFacebook(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile,.userFriends,.email], viewController: nil) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
                
                self.buttonFacebook.isHidden = true
                self.buttonLogout.isHidden = false
                self.buttonPlay.isHidden = false
                self.labelConnect.isHidden = true
                
                let params = ["fields":"id, name, email"]
                let graphRequest = GraphRequest(graphPath: "me", parameters: params)
                graphRequest.start { (urlResponse, requestResult) in
                    switch requestResult {
                    case .failed(let error):
                        print("error in graph request:", error)
                        break
                        // Handle the result's error
                        
                    case .success(let graphResponse):
                        if let responseDictionary = graphResponse.dictionaryValue {
                            let facebook_id = responseDictionary["id"]
                            var email = ""
                            let name = responseDictionary["name"]!
                            var the_os_id = ""
                            if (responseDictionary["email"]) != nil {
                                email = responseDictionary["email"] as! String
                            }
                            if (self.defaults.string(forKey: "os_id")) != nil {
                                the_os_id = self.defaults.string(forKey: "os_id")!
                            }
                            let os_id = the_os_id
                            self.defaults.set(facebook_id, forKey: "facebook_id")
                            self.defaults.set(email, forKey: "email")
                            self.defaults.set(name, forKey: "name")
                            
                            let parameters: Parameters = [
                                "name": name,
                                "email": email,
                                "facebook_id": facebook_id!,
                                "os_id": os_id
                            ]
                            print(parameters)
                            Alamofire.request("http://conecode.com/fives/users.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
                                print("Request: \(String(describing: response.request))")   // original url request
                                print("Response: \(String(describing: response.response))") // http url response
                                print("Result: \(response.result)")
                                
                                if let json = response.result.value {
                                    print("JSON: \(json)") // serialized json response
                                }
                                
                                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                    print("Data: \(utf8Text)") // original server data as UTF8 string
                                }
                            }
                        }
                    }
                }
                
                self.defaults.set(true, forKey: "facebook")
                self.defaults.set(accessToken.authenticationToken, forKey: "accessToken")
            }
        }
    }
    
    // Log out of Facebook
    @IBAction func logoutFacebook(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logOut()
        self.buttonFacebook.isHidden = false
        self.buttonLogout.isHidden = true
        self.buttonPlay.isHidden = true
        self.labelConnect.isHidden = false
        defaults.set(false, forKey: "facebook")
    }
    
    // open the Play View
    @IBAction func openPlay(_ sender: Any) {
        let vc:NewGameViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewGameViewController") as UIViewController as! NewGameViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    // open the How to Play View
    @IBAction func openHowTo(_ sender: Any) {
        let vc:HowToPlayViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HowToPlayViewController") as UIViewController as! HowToPlayViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    // Open Remove Ads view
    @IBAction func openRemoveAds(_ sender: UIButton) {
        let vc:PurchaseViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PurchaseViewController") as UIViewController as! PurchaseViewController
        
        self.present(vc, animated: true, completion: nil)
    }
}

