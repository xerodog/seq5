//
//  InviteFriendViewController.swift
//  Seq5
//  Loads contacts and will invite friends to download the app.
//
//  Created by Christopher Serra on 2/8/18.
//  Copyright © 2018 Christopher Serra. All rights reserved.
//

import UIKit
import Contacts
import Alamofire

class InviteFriendViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var contacts:Array<Dictionary<String, String>>! = []
    let contactStore = CNContactStore()
    var rowsWhichAreChecked = [NSIndexPath]()
    var emails:Array<String> = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonInvite: UIButton!
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // contacts settings
        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                    CNContactEmailAddressesKey] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        
        // get only contacts with email addresses
        do {
            try self.contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                if contact.emailAddresses.count > 0 {
                    var dictContact:Dictionary<String,String> = [:]
                    let firstName:String = contact.givenName as String
                    let lastName:String = contact.familyName as String
                    let email:String = contact.emailAddresses[0].value as String
                    dictContact["name"] = firstName+" "+lastName
                    dictContact["email"] = email
                    self.contacts.append(dictContact)
                }
            }
            
            // sort contacts by their name
            self.contacts = self.contacts.sorted{
                (($0 as Dictionary<String, AnyObject>)["name"] as! String) < (($1 as Dictionary<String, AnyObject>)["name"] as! String)
            }
            
            // reload table view
            self.tableView.reloadData()
        }
        catch {
            print("unable to fetch contacts")
        }
        
        // allow user to select multiple contacts
        self.tableView.allowsMultipleSelection = true
    }
    
    // Table View methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactCell
        cell.textLabel?.text = self.contacts![indexPath.row]["name"]
        
        // if contact email in list of selected emails, add a checkmark (to prevent scrolling selection issues)
        if emails.contains(self.contacts![indexPath.row]["email"]!) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        // when row is deselected, remove their email from array of emails
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
            self.emails = self.emails.filter{$0 != self.contacts![indexPath.row]["email"]!}
        }

        // if there aren't any rows selected, hide the invite button
        if self.emails.count > 0 {
            self.buttonInvite.isHidden = false
        } else {
            self.buttonInvite.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // when row is selected, add their email to array of emails
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            emails.append(self.contacts![indexPath.row]["email"]!)
        }
        
        // if there aren't any rows selected, hide the invite button
        if self.emails.count > 0 {
            self.buttonInvite.isHidden = false
        } else {
            self.buttonInvite.isHidden = true
        }
    }
    
    // API call the send array of emails to server to send the contacts an invite.
    @IBAction func inviteFriends(_ sender: Any) {
        let parameters: Parameters = [
            "facebook_id": self.defaults.string(forKey: "facebook_id")!,
            "emails": self.emails
        ]
        
        Alamofire.request("http://conecode.com/fives/email.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            print("Response: \(String(describing: response.result.value))") // http url response
            if let error = response.result.error {
                print(error)
            } else {
                self.closeView(response)
            }
        }
    }
    
    // close the current view
    @IBAction func closeView(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class ContactCell: UITableViewCell {
}
